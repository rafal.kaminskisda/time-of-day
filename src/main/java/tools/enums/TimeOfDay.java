package tools.enums;

public enum TimeOfDay {
    NIGHT,
    MORNING,
    AFTERNOON,
    EVENING
}
